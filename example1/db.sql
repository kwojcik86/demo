/**
Wyświetl imię, nazwisko i inicjały każdego klienta. Wynik posortuj względem inicjałów, nazwiska i imienia klienta. 
*/
SELECT
  CONCAT(SUBSTR(imie, 1, 1), SUBSTR(nazwisko, 1, 1)) AS inc,
  imie,
  nazwisko
FROM klienci
ORDER BY inc, imie, nazwisko


/**
Utwórz widok o nazwie klient_raport zawierający informacje o ilości zamówień każdego z klientów (id_klient, imie, nazwisko).
Uwzględnij klientów, którzy nie kupili żadnego produktu.
Za pomocą utworzonego widoku znajdź́ klientów, którzy złożyli zamówienie więcej niż jeden raz.
*/
CREATE VIEW klient_raport AS
  SELECT k.id_klient, k.imie, k.nazwisko, count(z.id_zamowienia) as cnt
  FROM klienci as k
  LEFT JOIN zamowienia AS z ON k.id_klient = z.id_klient;

SELECT * FROM klient_raport where cnt > 1;

/*
Napisać procedurę o nazwie wypisz_produkty, która posiada tylko jeden parametr – typ_produktu.
Procedura powinna wyświetlać wszystkie informacje z tabeli produkty o produktach danego typu.
*/
DELIMITER //

CREATE DEFINER=`kinguin`@`%` PROCEDURE `wypisz_produkty`(
  IN typ_produktu VARCHAR(255)
)
  BEGIN

    SELECT * from produkty
    WHERE
      typ = typ_produktu;
  END //

DELIMITER ;

/*
Napisać funkcję o nazwie aktywnosc_klienta, która będzie zwracać ilość zamówionych produktów
dla klienta o identyfikatorze zadanym jako parametr funkcji.
*/
DELIMITER //
CREATE FUNCTION aktywnosc_klienta(CID INT)
  RETURNS INT
DETERMINISTIC
  BEGIN
    DECLARE cnt INT;

    SELECT SUM(p.ilosc)
    INTO cnt
    FROM zamowienia z
      LEFT JOIN produkty p ON z.id_produkt = p.id_produkt
    WHERE z.id_klienta = CID;

    RETURN cnt;
  END//


/*
Utwórz unikalny indeks, wspólny dla kolumn nazwisko i nip w tabeli klienci.
*/
ALTER TABLE klienci ADD UNIQUE unique_index_nazwnip (nazwisko, nip);

/*
Usunąć klientów, którzy nie zamówili żadnego produktu.
*/
DELETE FROM klienci WHERE id_klient NOT IN ( SELECT DISTINCT id_klient FROM zamowienia)

/*
Zmień domyślny format daty na dzień/miesiąc/rok.
**/
SELECT DATE_FORMAT(NOW(), "%d/%m/%Y") AS date


/**
    Przyjmijmy, że tabela MY_INTEGERS została utworzona poniższym poleceniem i zawiera niepowtarzające się liczby naturalne.

create table MY_INTEGERS (

  my_id integer not null

);



Napisz zapytanie, które zwróci najmniejszą wolną liczbę.

Przykład: Tabela zawiera liczby: 1, 2, 4, 5, 10, 15, zwraca liczbę 3.
 */

SELECT t1.id + 1 AS missing
FROM MY_INTEGERS AS t1
  LEFT JOIN MY_INTEGERS AS t2 ON t1.id + 1 = t2.id
WHERE t2.id IS NULL
ORDER BY t1.id
LIMIT 1;
