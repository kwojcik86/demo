<?php

namespace App;


class Basic
{
    protected $_colors = ['red', 'green', 'blue', 'white', 'black', null];
    protected $_basePath = '/../products/';

    public function generateRandomItems(int $count): array
    {
        $products = new Products();
        $ids = [];
        $this->_basePath = realpath(__DIR__ . $this->_basePath);
        for ($i = 1; $i <= $count; $i++) {
            $color = null;
            $data = $this->_generateRandomData($i);
            try {
                $class = 'App\Product';
                if ($i % 2 == 1) {
                    $class = 'App\ProductVariation';
                    $color = $this->_getRandomColor();
                    if (is_null($color)) {
                        throw new \Exception('Color is not allowed');
                    }
                    $data = $data + ['color' => $color];
                }
            } catch (\Exception $e) {
                echo $e->getMessage() . "\n";
                continue;
            }
            $id = uniqid();
            $ids[] = $id;
            $filename = $this->_basePath . DIRECTORY_SEPARATOR . $id;
            file_put_contents($filename, json_encode($data));
            $products->append(new $class($filename, $color));
        };

        return $ids;
    }

    protected function _generateRandomData(int $i): array
    {
        return [
            'id'       => $i,
            'name'     => md5(microtime()),
            'price'    => (rand(1, 10000000) / 100),
            'quantity' => rand(1, 100)
        ];
    }

    protected function _getRandomColor() //:string
    {
        shuffle($this->_colors);

        return $this->_colors[array_rand($this->_colors)];
    }

    public function showItems(string $id)
    {
        $result = null;

        try {
            $result = new ProductVariation($id);
        } catch (\Exception $e) {
        }finally {
            if (!($result instanceof ProductVariation)) {
                $result = new Product($id);
            }
        }
        echo $result;
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    public function deleteProducts(string $id): bool
    {
        $path = $this->_basePath . $id;
        if (file_exists($path)) {
            return unlink($path);
        }

        return false;
    }
}
