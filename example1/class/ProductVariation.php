<?php

namespace App;


class ProductVariation extends Product
{
    protected $_color;

    /**
     * ProductVariation constructor.
     *
     * @param string $file
     *
     * @throws ProductFileNotFound
     * @throws UndefinedVariantColor
     */
    public function __construct(string $file)
    {
        parent::__construct($file);

        if (empty($this->_json['color'])) {
            throw new UndefinedVariantColor();
        }
        $this->setColor($this->_json['color']);

        return $this;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->_color;
    }

    /**
     * @param string $color
     *
     * @return ProductVariation
     */
    public function setColor(string $color): namespace\ProductVariation
    {
        $this->_color = $color;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return json_encode(['id: ' . $this->getId(),
                            'name'=>$this->getName(),
                            'price'=>$this->getPrice(),
                            'quantity'=>$this->getQuantity(),
                            'color'=>$this->getColor()]);
    }
}