<?php

namespace App;


class Product implements Item
{
    protected static $_vat = 123;
    protected $_id;
    protected $_name;
    protected $_price;
    protected $_quantity;
    protected $_amount = null;
    protected $_json = null;

    /**
     * Product constructor.
     *
     * @param $file
     */
    public function __construct(string $file)
    {
        if (!file_exists($file)) {
            throw new ProductFileNotFound();
        }
        $this->_json = json_decode(file_get_contents($file),true);

        $this->setId(intval($this->_json['id']))
             ->setName($this->_json['name'])
             ->setPrice(floatval($this->_json['price']))
             ->setQuantity(intval($this->_json['quantity']));

        return $this;
    }

    /**
     * @return float
     */
    public function getNet(): float
    {
        return round((($this->_price * 100 / self::$_vat) / 100), 2);
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        if (!is_null($this->_amount)) {
            return $this->_amount;
        }

        $this->_amount = $this->_price * $this->_quantity;

        return $this->_amount;
    }

    public function __toString(): string
    {
        return json_encode(['id: ' . $this->getId(),
               'name'=>$this->getName(),
               'price'=>$this->getPrice(),
               'quantity'=>$this->getQuantity()]);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->_id;
    }

    /**
     * @param int $id
     *
     * @return Product
     */
    public function setId(int $id): namespace\Product
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->_name;
    }

    /**
     * @param string $name
     *
     * @return Product
     */
    public function setName(string $name): namespace\Product
    {
        $this->_name = $name;

        return $this;
    }

    /**
     * @return float
     *
     * @return Product
     */
    public function getPrice(): float
    {
        return $this->_price;
    }

    /**
     * @param float $price
     *
     * @return Product
     */
    public function setPrice(float $price): namespace\Product
    {
        $this->_price = $price;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->_quantity;
    }

    /**
     * @param int $quantity
     *
     * @return Product
     */
    public function setQuantity(int $quantity): namespace\Product
    {
        $this->_quantity = $quantity;

        return $this;
    }
}