<?php

namespace App;


use Mockery\Exception;

class Products extends \ArrayIterator
{
    public function append($value)
    {
        if(!($value instanceof Item))
        {
            throw new Exception('Append value must be instance of App\Item');
        }
        return parent::append($value);
    }

    public function __toString()
    {
        return '';//return $this->serialize();
    }
}