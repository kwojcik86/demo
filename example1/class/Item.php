<?php

namespace App;

interface Item
{
    public function getId(): int;

    public function getNet(): float;

}