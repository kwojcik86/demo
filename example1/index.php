<?php

spl_autoload_register(
    function ($class) {
        if (substr($class, 0, 4) == 'App\\') {
            $class = substr($class, 4);
        }

        require_once 'class/' . $class . '.php';
    }
);

$app = new App\Basic();

/**
 * command line
 */
if (defined('STDIN') && isset($argv['1'])) {
    $arg = $argv['1'];
    if ($arg == 'generate') {
        $app->generateRandomItems(10);
    } else {
        $success = $app->deleteProducts($arg);
        echo str_pad('Deleting file ' . $arg, 45, '.') . (($success) ? 'SUCCESS' : 'FAIL') . "\n";
    }
} else {
    echo '<pre>';
    foreach (glob('./products/*') as $filename) {
        echo str_pad($filename,50,'*', STR_PAD_BOTH)."\n";
        echo $app->showItems(__DIR__ . DIRECTORY_SEPARATOR . $filename) . "\n\n\n";
    }

}