<?php
require __DIR__ . '/vendor/autoload.php';

class LottoClawler
{
    protected $_saveToDatabase;
    protected $_results = [];
    protected $_urls = [
        'elgrodo'     => 'https://www.elgordo.com/results/euromillonariaen.asp',
        'lotto'       => 'https://www.lotto.pl/lotto/wyniki-i-wygrane',
        'eurojackpot' => 'https://www.lotto.pl/eurojackpot/wyniki-i-wygrane',
    ];

    public function run(bool $saveToDatabase = false)
    {
        $this->_saveToDatabase = $saveToDatabase;

        $this->fetchElgrodo();
        $this->fetchLotto();
        $this->fetchEurojackpot();

        /**
         * save data to json file
         */
        try {
            $encoded = json_encode($this->_results);
            file_put_contents('./data/' . time() . '.json', $encoded);
        } catch (Exception $e) {
            echo 'Error while saving fetched data. Message: ' . $e->getMessage();
        }

        if ($this->_saveToDatabase) {
            $this->saveToDatabase();
        }

        echo 'Done.' . "\n";
    }

    protected function fetchElgrodo(): array
    {
        $this->_results['elgrodo'] = [];
        try {
            $html = $this->fetchHtml($this->_urls['elgrodo']);
            $c = Wa72\HtmlPageDom\HtmlPageCrawler::create($html);
            $tr = $c->filter('.combi.balls');

            //basic data
            $tr->filter('.num')->each(
                function (Wa72\HtmlPageDom\HtmlPageCrawler $node, $i) {
                    $this->_results['elgrodo'][] = $node->first()->text();
                }
            );

            //esp data
            $tr->filter('.esp')->each(
                function (Wa72\HtmlPageDom\HtmlPageCrawler $node, $i) {
                    $this->_results['elgrodo'][] = [
                        'num' => $node->filter('.int-num')->first()->text(),
                        'txt' => $node->filter('.txt-num')->first()->text(),
                    ];
                }
            );
        } catch (Exception $e) {
        } finally {
            return $this->_results['elgrodo'];
        }
    }

    protected function fetchHtml(string $url): string
    {
        $curl = new Curl\Curl();
        $curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
        $curl->get($url);
        $curl->close();

        if ($curl->error) {
            throw new Exception('Error while fetching ' . $url . ', errorCode:' . $curl->error_code);
        } else {
            return $curl->response;
        }
    }

    protected function fetchLotto(): array
    {
        $this->_results['lotto'] = [];
        try {
            $html = $this->fetchHtml($this->_urls['lotto']);
            $c = Wa72\HtmlPageDom\HtmlPageCrawler::create($html);
            $tr = $c->filter('div.resultsItem.lotto')->first()->filter('div.resultnumber div');
            $tr->each(
                function (Wa72\HtmlPageDom\HtmlPageCrawler $node, $i) {
                    $this->_results['lotto'][] = $node->filter('div.number')->text();
                }
            );
        } catch (Exception $e) {
        } finally {
            return $this->_results['lotto'];
        }
    }

    protected function fetchEurojackpot(): array
    {
        $this->_results['eurojackpot'] = [];
        try {
            $html = $this->fetchHtml($this->_urls['eurojackpot']);
            $c = Wa72\HtmlPageDom\HtmlPageCrawler::create($html);
            $tr = $c->filter('div.resultsItem.euroJackpot')->first()->filter('div.resultnumber div');
            $this->_results['eurojackpot']['base'] = [];
            $tr->each(
                function (Wa72\HtmlPageDom\HtmlPageCrawler $node, $i) {
                    $value = $node->filter('div.number')->text();

                    if ($value == '+') {
                        $this->_results['eurojackpot']['advantage'] = [];

                        return false;
                    }
                    $keys = array_keys($this->_results['eurojackpot']);
                    $key = end($keys);
                    $this->_results['eurojackpot'][$key][] = $value;
                }
            );
        } catch (Exception $e) {
        } finally {
            return $this->_results['eurojackpot'];
        }
    }

    public function saveToDatabase(): bool
    {
        $connection = new Opis\Database\Connection('sqlite:db.sqlite');

        $db = new Opis\Database\Database($connection);

        $db->insert(
            [
                'date' => (string)time(),
                'data' => serialize($this->_results)
            ]
        )->into('fetcheddata');

        return true;
    }
}

$saveToDatabase = false;
if (defined('STDIN')) {
    foreach ($argv as $arg) {
        $arg = explode('=', $arg);
        if ($arg['0'] === 'save-to-database' && $arg['1'] === 'true') {
            $saveToDatabase = true;
            break;
        }
    }
}

$crawler = new LottoClawler();
$crawler->run($saveToDatabase);
