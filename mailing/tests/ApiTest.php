<?php

namespace Tests;

class ApiTest extends BaseTestCase
{
    protected $_id;
    /**
     * Add someone to queue
     */
    public function testPost()
    {
        $data = ['name' => 'Jan Kowalski', 'mail' => time() . '@test.io'];
        $response = $this->runApp('POST', '/api/contact', $data);

        $response = (array)json_decode((string)$response->getBody());
        $this->_id = $response['id'];
        $this->assertArraySubset($data, $response);
        $this->edit();
    }
    /**
     * Add someone to queue
     */
    public function edit()
    {
        $data = ['name' => 'Jan Kowalski Edited', 'mail' => time() . '@test.io','info'=>microtime()];
        $response = $this->runApp('PUT', '/api/contact/'.$this->_id, $data);

        $response = (array)json_decode((string)$response->getBody());
        $this->assertArraySubset($data, $response);
    }

}
