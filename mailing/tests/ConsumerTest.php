<?php

namespace Tests;

class ConsumerTest extends BaseTestCase
{
    /**
     * is queue empty?
     */
    public function testQueue()
    {
        $response = $this->runApp('GET', '/api/queue');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('[]', (string)$response->getBody());
    }
}