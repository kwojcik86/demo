# Framework
I used Slim 3 MVC as a framework. My code is in `app/`. I used also official librares from mailgun and sendgrid

# How to run it - commands
1. Install php
2. Install composer (`https://getcomposer.org/doc/00-intro.md`)
3. `$ cd mailer`
4. `$ composer update`
5. `$ php -S 0.0.0.0:8888 -t public/`
6. Browse to http://localhost:8888
7. Consumer (sending emails) is avalaible at http://localhost:8888/api/queue

# Security
App doesn't have any authorization system.

# Scalability
App's an imitation of the queue system like `RabbitMQ`. Part of API is as `producer`, another part as `consumer` . In my opinion instance with RabbitMQ instead of sqlite is better in production.

# Logging
App has small loggin-system - logs are at `logs` dir. Every knows errors are captured.
 
# Testing
A small example of tests has been implemented and can be run by `./vendor/bin/phpunit ./test/ConsumerTest.php` or `./vendor/bin/phpunit ./test/ApiTest.php`
