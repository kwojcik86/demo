<?php
// Routes

$app->get('/', 'App\Controller\HomeController:dispatch')
    ->setName('homepage');


$app->get('/api/contacts[/]', 'App\Controller\ApiController:listAll');
$app->post('/api/contact[/]', 'App\Controller\ApiController:post');
$app->put('/api/contact/{id:\d+}[/]', 'App\Controller\ApiController:put');
$app->delete('/api/contact/{id:\d+}[/]', 'App\Controller\ApiController:delete');

$app->get('/api/queue[/]', 'App\Controller\ConsumerController:queue');