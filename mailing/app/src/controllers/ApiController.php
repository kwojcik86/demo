<?php
namespace App\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Model\Queue as Queue;

final class ApiController extends BaseController
{
    public function listAll(Request $request, Response $response, $args)
    {
        $this->logger->info("Api::list dispatched");

        try {
            $data = $this->em->getRepository('App\Model\Queue')->findAll();
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
        }

        $data = array_map(
            function ($u) {
                return $u->getArrayCopy();
            },
            $data
        );
        return $response->withJson($data)->withHeader('Content-Type', 'application/json');
    }

    public function delete(Request $request, Response $response, $args)
    {
        $this->logger->info("Api::delete dispatched");

        try {
            $data = $this->em->find('App\Model\Queue', intval($args['id']));
            if (is_null($data)) {
                throw new \Exception('Record #'.intval($args['id']) .' not found');
            }

            $this->em->remove($data);
            $this->em->flush();
            return $response;
        } catch (\Exception $e) {
            $response->getBody()->write($e->getMessage());
            return $response
                ->withStatus(406, $e->getMessage())
                ->withHeader('Content-Type', 'application/json');
            die;
        }
    }

    public function post(Request $request, Response $response, $args)
    {
        $this->logger->info("Api::create dispatched");
        try {
            $data = new Queue();

            $data
                ->setName($request->getParam('name'))
                ->setMail($request->getParam('mail'))
                ->setInfo($request->getParam('info'));

            $this->validate($data);

            $this->em->persist($data);
            $this->em->flush();
            return $response->withJson($data->getArrayCopy())->withHeader('Content-Type', 'application/json');
        } catch (\Exception $e) {
            $response->getBody()->write($e->getMessage());

            return $response
                ->withStatus(406, $e->getMessage())
                ->withHeader('Content-Type', 'application/json');
            die;
        }
    }

    public function put(Request $request, Response $response, $args)
    {
        $this->logger->info("Api::edit dispatched");
        try {
            $data = $this->em->find('App\Model\Queue', intval($args['id']));
            if (is_null($data)) {
                throw new \Exception('Record #'.intval($args['id']) .' not found');
            }
            $data
                ->setName($request->getParam('name'))
                ->setMail($request->getParam('mail'))
                ->setInfo($request->getParam('info'));

            $this->validate($data);

            $this->em->persist($data);
            $this->em->flush();
            return $response->withJson($data->getArrayCopy())->withHeader('Content-Type', 'application/json');
        } catch (\Exception $e) {
            $response->getBody()->write($e->getMessage());
            return $response
                ->withStatus(406, $e->getMessage())
                ->withHeader('Content-Type', 'application/json');
            die;
        }
    }

    protected function validate($queue)
    {

        $validator = \Symfony\Component\Validator\Validation::createValidatorBuilder()
                                                            ->enableAnnotationMapping()
                                                            ->getValidator();

        $violations = $validator->validate($queue);
        if($violations->has(0))
            throw new \Exception($violations->get(0)->getMessage());

        return true;
    }
}
