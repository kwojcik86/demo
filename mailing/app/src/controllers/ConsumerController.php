<?php

namespace App\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Model\Queue as Queue;

final class ConsumerController extends BaseController
{
    protected $_mailProvider = null;
    protected $_failsProviders = [];

    public function queue(Request $request, Response $response, $args)
    {
        $this->logger->info("Api::queue dispatched");
        try {
            $items = $this->em->getRepository('App\Model\Queue')
                              ->createQueryBuilder('t')
                              ->where('t.processed is null')
                              ->getQuery()
                              ->getResult();
            $data = [];
            foreach ($items as $item) {
                $logs = $this->sendMail($item);

                foreach ($logs as $log) {
                    $this->logger->info("Api::queue: " . $log);
                }

                $data = array_merge($data, $logs);
            }

            return $response->withJson($data)
                            ->withStatus(200)
                            ->withHeader('Content-Type', 'application/json');
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
        }
    }

    protected function sendMail(Queue $queue): array
    {
        $provider = $this->getProvider(true);
        $msg = [];
        $result = false;
        do {
            try {
                $info = 'Send using provider ' . $provider . ' to ' . $queue->getMail();
                $result = $this->sendByProvider($queue);
            } catch (\Exception $e) {
                $info .= '. Error occurred: ' . $e->getMessage() . '.';
                $provider = $this->getProvider(false, true);
            }
            $msg[] = $info;
            if (is_null($provider)) {
                break;
            }
        } while (!$result);

        if ($result) {
            $queue
                ->setProvider($provider)
                ->setProcessed(new \DateTime('now'));

            $this->em->persist($queue);
            $this->em->flush();
        }

        return $msg;
    }

    protected function getProvider($cleanFails = false, $reload = false)
    {
        if ($cleanFails) {
            $this->_failsProviders = [];
        }

        if ($reload && !is_null($this->_mailProvider)) {
            $this->_failsProviders[] = $this->_mailProvider;
            $this->_mailProvider = null;
        }

        if (is_null($this->_mailProvider)) {
            foreach ($this->settings['mail_services'] as $providerKey => $settings) {
                if ($this->_mailProvider != $providerKey && !in_array($providerKey, $this->_failsProviders)
                    && method_exists($this, 'sendMail' . ucfirst($providerKey))) {
                    $this->_mailProvider = $providerKey;
                    break;
                }
            }
        }

        return $this->_mailProvider;
    }

    protected function sendByProvider(Queue $queue)
    {
        $provider = $this->getProvider();

        return $this->{'sendMail' . ucfirst($provider)}($queue);
    }

    protected function sendMailSendgrid(Queue $queue): bool
    {
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($this->settings['mail']['from']['mail'], $this->settings['mail']['from']['name']);
        $email->setSubject($this->settings['mail']['subject']);
        $email->addTo($queue->getMail(), $queue->getName());
        $email->addContent('text/plain', $this->settings['mail']['message']);

        $sendgrid = new \SendGrid($this->settings['mail_services']['sendgrid']['api_key']);

        $response = $sendgrid->send($email);
        if ($response->statusCode() != 202 && !empty($response->body())) {
            throw new \Exception($response->body());
        }

        return true;
    }

    protected function sendMailMailgun(Queue $queue): bool
    {
        $mg = \Mailgun\Mailgun::create($this->settings['mail_services']['mailgun']['api_key']);
        $mg->messages()->send(
            $this->settings['mail_services']['mailgun']['domain'], [
            'from' => $this->settings['mail']['from']['mail'],
            'to' => $queue->getMail(),
            'subject' => $this->settings['mail']['subject'],
            'text' => $this->settings['mail']['message']
        ]
        );

        return true;
    }
}
