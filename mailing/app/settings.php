<?php
return [
    'settings' => [
        // comment this line when deploy to production environment
        'displayErrorDetails' => true,
        // View settings
        'view' => [
            'template_path' => __DIR__ . '/templates',
            'twig' => [
                'cache' => __DIR__ . '/../cache/twig',
                'debug' => true,
                'auto_reload' => true,
            ],
        ],

        // doctrine settings
        'doctrine' => [
            'meta' => [
                'entity_path' => [
                    __DIR__ . '/src/models'
                ],
                'auto_generate_proxies' => true,
                'proxy_dir' =>  __DIR__.'/../cache/proxies',
                'cache' => null,
            ],
            'connection' => [
              'driver' => 'pdo_sqlite',
              'path' => __DIR__.'/../sql/db.sqlite'
            ]
        ],

        // monolog settings
        'logger' => [
            'name' => 'app',
            'path' => __DIR__ . '/../log/app.log',
        ],

        'mail' => [
            'from' => ['mail'=>'kwojcik@test.com', 'name'=>'TestAPI'],
            'subject' => 'Coding Subject',
            'message' => 'simple message',
        ],

        'mail_services' => [
            'mailgun' => [
				'domain' => 'sandboxffa5a747cbe640209fa2b10dc67588f7.mailgun.org',
				'api_key' => '728b9f039f8110b779568101666536eb-8b7bf2f1-33e99427',
			],
			'sendgrid' => [
				'api_key' => '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'
			]
        ],
    ],
];
